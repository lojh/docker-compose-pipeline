package main

import (
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message":     os.Getenv("PONG_MESSAGE"),
			"servicename": os.Getenv("SERVICE_NAME"),
			"revision":    os.Getenv("CI_COMMIT_SHORT_SHA"),
		})
	})
	r.Run() // listen and serve on 0.0.0.0:8080
}
