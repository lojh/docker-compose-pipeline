# Basic dockerfile used to run golang code
FROM golang:alpine3.10 AS build

# Copy src files to build folder
COPY main.go /app/src/
COPY go.mod /app/src/
COPY go.sum /app/src/

# Use workdir instead of cd
WORKDIR /app/src/

# Ensure git is present
RUN apk add --no-cache git=2.22.0-r0

# Get dependancies - will also be cached if we won't change mod/sum
RUN go mod download

# Build hello world - Disabling CGO allows usage of scratch
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o containerstoday

# Use scratch as base for runtime image
FROM scratch

COPY --from=build /app/src/containerstoday /usr/local/containerstoday

EXPOSE 8080

ENTRYPOINT [ "/usr/local/containerstoday" ]